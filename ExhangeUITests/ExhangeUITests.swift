//
//  ExhangeUITests.swift
//  ExhangeUITests
//
//  Created by Hady Nourallah on 19/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import XCTest

class ExhangeUITests: XCTestCase {
    var app: XCUIApplication!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        
        // We send a command line argument to our app,
        // to enable it to reset its state
        app.launchArguments.append("--uitesting")
        app.launch()
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testHappyPath() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        homeViewControllerUIEmpty()
        addExchangeRate()
        sleep(3)
        checkAfterAddingEURUSD()
        addSameExchangeRate()
        checkAfterAddingEURUSD()
        addUSDAUDRate()
        sleep(3)
        checkAfterAddingTwoPairs()
        deleteCellPair("USDAUD")
        sleep(3)
        checkAfterAddingEURUSD()
        deleteCellPair("EURUSD")
        sleep(3)
        homeViewControllerUIEmpty()


    }

    func homeViewControllerUIEmpty() {
        app = XCUIApplication()
        XCTAssertTrue(app.otherElements["HomeViewController"].exists)
        let tables = app.tables
        let emptyCell = tables.cells["HomeTableCellEmpty"]
        XCTAssertTrue(emptyCell.exists)
        
        XCTAssertTrue(emptyCell.staticTexts["Add currency pair"].exists)
        XCTAssertTrue(emptyCell.staticTexts["Choose a currency pair to compare their live rates"].exists)
    }
    
    func addExchangeRate() {
        app = XCUIApplication()
        let cell = app.tables.cells["HomeTableCellEmpty"]
        cell.tap()
        XCTAssertTrue(app.otherElements["AddExchangeViewController"].exists)
        let navigation = app.navigationBars["Add Exchange"]
        XCTAssertTrue(navigation.exists)
        XCTAssertTrue(navigation.buttons["Stop"].exists)
        navigation.buttons["Stop"].tap()
        
        homeViewControllerUIEmpty()
        cell.tap()
        XCTAssertTrue(app.otherElements["AddExchangeViewController"].exists)
        XCTAssertTrue(navigation.exists)
        sleep(3)
        XCTAssertEqual(app.tables.cells.count, 33)
        let eurCell = app.cells.containing(.staticText, identifier: "Euro").firstMatch
        let usdCell = app.cells.containing(.staticText, identifier: "US Dollar").firstMatch
        eurCell.tap()
        
        XCTAssertTrue(navigation.buttons["Stop"].exists)
        XCTAssertTrue(navigation.buttons["Back"].exists)
        navigation.buttons["Back"].tap()
        
        XCTAssertFalse(eurCell.otherElements["ExchangeSelectedLayerEuro"].exists)
        eurCell.tap()
        
        let selectedLayer = app.cells.containing(.staticText, identifier: "Euro").firstMatch.otherElements["ExchangeSelectedLayerEuro"]
        self.waitForElementToAppear(element: selectedLayer, timeout: 10)

        usdCell.tap()
    }
    
    func checkAfterAddingEURUSD() {
        app = XCUIApplication()
        XCTAssertTrue(app.otherElements["HomeViewController"].exists)
        let cells = app.tables.cells
        let headerCell = cells["HomeTableCellHeader"]
        let EURUSDCell = cells["HomeTableCell: EURUSD"]

        waitForElementToAppear(element: headerCell)

        XCTAssertEqual(cells.count, 2)
        
        XCTAssertTrue(headerCell.exists)
        XCTAssertTrue(headerCell.staticTexts["Add currency pair"].exists)
        
        XCTAssertTrue(EURUSDCell.exists)
        XCTAssertTrue(EURUSDCell.staticTexts["Euro · EUR"].exists)
        XCTAssertTrue(EURUSDCell.staticTexts["US Dollar · USD"].exists)
    }
    
    func addSameExchangeRate() {
        app = XCUIApplication()
        app.tables.cells["HomeTableCellHeader"].tap()
        let eurCell = app.cells.containing(.staticText, identifier: "Euro").firstMatch
        let usdCell = app.cells.containing(.staticText, identifier: "US Dollar").firstMatch
        eurCell.tap()
        usdCell.tap()
    }
    
    func addUSDAUDRate() {
        app = XCUIApplication()
        app.tables.cells["HomeTableCellHeader"].tap()
        let usdCell = app.cells.containing(.staticText, identifier: "US Dollar").firstMatch
        let audCell = app.cells.containing(.staticText, identifier: "Australian Dollar").firstMatch
        usdCell.tap()
        audCell.tap()
    }
    
    func checkAfterAddingTwoPairs() {
        app = XCUIApplication()
        let cells = app.tables.cells
        let headerCell = cells["HomeTableCellHeader"]
        let EURUSDCell = cells["HomeTableCell: EURUSD"]
        let USDAUDCell = cells["HomeTableCell: USDAUD"]
        
        waitForElementToAppear(element: USDAUDCell, timeout: 10)
        
        XCTAssertEqual(cells.count, 3)
        
        XCTAssertTrue(headerCell.exists)
        XCTAssertTrue(headerCell.staticTexts["Add currency pair"].exists)
        
        XCTAssertTrue(EURUSDCell.exists)
        XCTAssertTrue(EURUSDCell.staticTexts["Euro · EUR"].exists)
        XCTAssertTrue(EURUSDCell.staticTexts["US Dollar · USD"].exists)

        XCTAssertTrue(USDAUDCell.exists)
        XCTAssertTrue(USDAUDCell.staticTexts["Australian Dollar · AUD"].exists)
        XCTAssertTrue(USDAUDCell.staticTexts["US Dollar · USD"].exists)
    }
    
    func deleteCellPair(_ pair: String) {
        app = XCUIApplication()
        let cells = app.tables.cells
        let USDAUDCell = cells["HomeTableCell: \(pair)"]
        USDAUDCell.tap()
        
        let sheet = app.sheets.containing(.staticText, identifier: "Are you sure you want to delete \(pair)?").firstMatch
        
        XCTAssertTrue(sheet.exists)
        sheet.buttons["Cancel"].tap()
        sleep(3)
        XCTAssertFalse(sheet.exists)
        USDAUDCell.tap()
        app.sheets.buttons["Delete!"].tap()
    }
    
    private func waitForElementToAppear(element: XCUIElement, timeout: TimeInterval = 5,  file: String = #file, line: UInt = #line) {
        let existsPredicate = NSPredicate(format: "exists == true")
        
        expectation(for: existsPredicate,
                    evaluatedWith: element, handler: nil)
        
        waitForExpectations(timeout: timeout) { (error) -> Void in
            if (error != nil) {
                let message = "Failed to find \(element) after \(timeout) seconds."
                self.recordFailure(withDescription: message, inFile: file, atLine: Int(line), expected: true)
            }
        }
    }

}
