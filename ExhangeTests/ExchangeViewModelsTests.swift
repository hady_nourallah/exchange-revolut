//
//  ExchangeViewModelsTests.swift
//  ExhangeTests
//
//  Created by Hady Nourallah on 19/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import XCTest
@testable import Currencies


class ExchangeViewModelsTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    //MARK:- HomeViewController.ViewModel
    func testHomeViewControllerSetupCell() {
        let viewModel = HomeViewController.ViewModel()
        let exchangeModel: ExchangeModel! = ExchangeModel(pair: "USDEUR", value: 0.4)
        let exchangeModel1: ExchangeModel! = ExchangeModel(pair: "USDAUD", value: 1.4)
        let exchangeModel2: ExchangeModel! = ExchangeModel(pair: "AUDEUR", value: 2.4)

        viewModel.setupCells([])
        XCTAssertEqual(viewModel.cells, [HomeViewController.ViewModel.CellType.empty])
        
        viewModel.setupCells([exchangeModel, exchangeModel1, exchangeModel2].compactMap({$0}))
        let cells = [HomeViewController.ViewModel.CellType.header,
                     HomeViewController.ViewModel.CellType.cell(exchangeModel),
                     HomeViewController.ViewModel.CellType.cell(exchangeModel1),
                     HomeViewController.ViewModel.CellType.cell(exchangeModel2)]
        XCTAssertEqual(viewModel.cells, cells)
    }
    
    func testHomeViewModelDeletePairs() {
        Settings.selectedPairs = ["AUDEUR", "EURAUD", "USDEUR"]
        let viewModel = HomeViewController.ViewModel()
        viewModel.deletePair("AUDEUR")
        XCTAssertEqual(Settings.selectedPairs, ["EURAUD", "USDEUR"].sorted())
        viewModel.deletePair("")
        XCTAssertEqual(Settings.selectedPairs, ["EURAUD", "USDEUR"].sorted())
        viewModel.deletePair("Wrong pair")
        XCTAssertEqual(Settings.selectedPairs, ["EURAUD", "USDEUR"].sorted())
        viewModel.deletePair("EURAUD")
        XCTAssertEqual(Settings.selectedPairs, ["USDEUR"])
    }
    
    //MARK:- AddExchangeViewController.ViewModel
    func testAddExchangeViewModelSelectCode() {
        let viewModel = AddExchangeViewController.ViewModel()
        let string1 = "EUR"
        let string2 = "USD"
        let string3 = "AUD"
        
        XCTAssertEqual(viewModel.selected, "")
        let _ = viewModel.select(string1)
        XCTAssertEqual(viewModel.selected, string1)
        let _ = viewModel.select(string2)
        XCTAssertEqual(viewModel.selected, string1 + string2)
        let _ = viewModel.select(string2)
        XCTAssertEqual(viewModel.selected, string1 + string2)
        let _ = viewModel.select(string1)
        XCTAssertEqual(viewModel.selected, string1 + string2)
        let _ = viewModel.select(string3)
        XCTAssertEqual(viewModel.selected, string1 + string2 + string3)
        let _ = viewModel.select(string1)
        XCTAssertEqual(viewModel.selected, string1 + string2 + string3)
    }
    
    func testAddExchangeViewModelDeselectCodes() {
        let viewModel = AddExchangeViewController.ViewModel()
        let string1 = "EUR"
        let string2 = "USD"
        let string3 = "AUD"
        viewModel.selected = string1 + string2 + string3
        viewModel.dropLastSelectedCode()
        XCTAssertEqual(viewModel.selected, string1 + string2)
        let _ = viewModel.select(string3)
        XCTAssertEqual(viewModel.selected, string1 + string2 + string3)
        viewModel.dropLastSelectedCode()
        viewModel.dropLastSelectedCode()
        XCTAssertEqual(viewModel.selected, string1)
        let _ = viewModel.select(string3)
        XCTAssertEqual(viewModel.selected, string1 + string3)
        viewModel.dropLastSelectedCode()
        XCTAssertEqual(viewModel.selected, string1)
        viewModel.dropLastSelectedCode()
        XCTAssertEqual(viewModel.selected, "")
        viewModel.dropLastSelectedCode()
        XCTAssertEqual(viewModel.selected, "")
    }
    
    //MARK:- ExchangeModel
    func testExchangeModelInit() {
        let _ = Settings.currencies //load currencies
        let pair = "AUDEUR"
        let pair1 = "AUD"
        let pair2 = "Wrong pair"
        let pair3 = "USDEUR"
        
        let model1 = ExchangeModel(pair: pair, value: 0)
        XCTAssertNotNil(model1)
        XCTAssertEqual(model1?.from.code, "AUD")
        XCTAssertEqual(model1?.to.code, "EUR")
        
        let model2 = ExchangeModel(pair: pair1, value: 0)
        XCTAssertNil(model2)
        
        let model3 = ExchangeModel(pair: pair2, value: 0)
        XCTAssertNil(model3)

        let model4 = ExchangeModel(pair: pair3, value: 0)
        XCTAssertNotNil(model4)
        XCTAssertEqual(model4?.from.code, "USD")
        XCTAssertEqual(model4?.to.code, "EUR")
    }
    
    //MARK:- Extensions
    func testExtensionArrays() {
        let list1 = [1, 2, 3, 4]
        let list2 = [1, 2, 3, 4, 5]
        let list3 = ["1", "2", "3", "5"]
        let list4 = ["4", "3", "2", "1", "-1"]
        let list5 = [[1, 2, 3], [9, 8, 7]]
        
        let split1 = list1.split()
        let split2 = list2.split()
        let split3 = list3.split()
        let split4 = list4.split()
        let split5 = list5.split()
        
        XCTAssertEqual(split1.left, [1, 2])
        XCTAssertEqual(split1.right, [3, 4])
        
        XCTAssertEqual(split2.left, [1, 2])
        XCTAssertEqual(split2.right, [3, 4, 5])

        XCTAssertEqual(split3.left, ["1", "2"])
        XCTAssertEqual(split3.right, ["3", "5"])

        XCTAssertEqual(split4.left, ["4", "3"])
        XCTAssertEqual(split4.right, ["2", "1", "-1"])

        XCTAssertEqual(split5.left, [[1, 2, 3]])
        XCTAssertEqual(split5.right, [[9, 8, 7]])
    }
    
    func testDoubletoCurrencyFormat() {
        let d1: Double = 1.3
        let d2: Double = 4.3453
        let d3: Double = 5.1234
        let d4: Double = 90.89
        let d5: Double = 2222.0
        let d6: Double = 2222.496
        let d7: Double = 854154042.9854433
        
        let c1 = d1.toCurrencyFormat() //"130"
        let c2 = d2.toCurrencyFormat() //"435"
        let c3 = d2.toCurrencyFormat(decimalSeperator: ".") //"4.35"
        let c4 = d3.toCurrencyFormat(decimalSeperator: ", ")//"5, 12"
        let c5 = d4.toCurrencyFormat()//"9089"
        let c6 = d5.toCurrencyFormat(decimalSeperator: "; decimal=") //"2,222; decimal=00"
        let c7 = d6.toCurrencyFormat(decimalSeperator: "; decimal=") // "2,222; decimal=50"
        let c8 = d7.toCurrencyFormat(decimalSeperator: ".") //"854,154,042cc99"
        
        XCTAssertEqual(c1, "130")
        XCTAssertEqual(c2, "435")
        XCTAssertEqual(c3, "4.35")
        XCTAssertEqual(c4, "5, 12")
        XCTAssertEqual(c5, "9089")
        XCTAssertEqual(c6, "2,222; decimal=00")
        XCTAssertEqual(c7, "2,222; decimal=50")
        XCTAssertEqual(c8, "854,154,042.99")
    }
}
