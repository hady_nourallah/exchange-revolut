//
//  ExchangeServiceTest.swift
//  ExhangeTests
//
//  Created by Hady Nourallah on 19/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import XCTest
@testable import Currencies

class ExchangeServiceTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAPIURL() {
        let pairs = ["AUDEUR", "EURAUD"]
        let expectedHOST = "europe-west1-revolut-230009.cloudfunctions.net"
        let expectedPath = "/revolut-ios"
        let request = RestAPI.getExchange(pairs).request
        XCTAssertEqual(expectedHOST, request?.url?.host)
        XCTAssertEqual(expectedPath, request?.url?.path)
        if let urlString = request?.url?.absoluteString {
            let urlComponents = URLComponents(string: urlString)
            let params = urlComponents?.queryItems?.compactMap({$0.value})
            XCTAssertEqual(params?.sorted(), pairs.sorted())
        } else {
            XCTFail("issue with pairs")
        }
    }
    
    func testAPICallWithSuccessfulPairs() {
        let pairs: [String] = ["AUDEUR", "EURAUD"]
        let expectation = XCTestExpectation(description: "API call success")

        Service().getCurrentExchange(pairs) { (response, errors) in
            XCTAssertNil(errors)
            XCTAssertEqual(pairs.sorted(), response?.map({$0.name}).sorted())
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testAPICallwithEmptyPairs() {
        let expectation = XCTestExpectation(description: "API call success")
        
        let pairs: [String] = []
        Service().getCurrentExchange(pairs) { (response, errors) in
            XCTAssertNil(errors)
            XCTAssertEqual(pairs.sorted(), response?.map({$0.name}).sorted())
            expectation.fulfill()

        }

        wait(for: [expectation], timeout: 1.0)
    }
    
    func testAPICallwithWrongPairs() {
        let expectation = XCTestExpectation(description: "API call success")
        
        let pairs: [String] = ["EUREGP"]
        Service().getCurrentExchange(pairs) { (response, errors) in
            XCTAssert(errors != nil)
            XCTAssert(response == nil)
            expectation.fulfill()
            
        }
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testErrorHandlingNotAcceptedCode() {
        let noInternetError = NSError(domain: NSURLErrorDomain, code: NSURLErrorNotConnectedToInternet, userInfo: nil)
        let randomError = NSError(domain: "Random Error from API", code: -2349, userInfo: nil)
        let acceptedCodeError: NSError = NSError(domain: "return 200 but some other errior occured", code: 200, userInfo: nil)
        let statusCode = Int.random(in: 300...500)
        let urlResponse = HTTPURLResponse(url: RestAPI.getExchange([]).url!, statusCode: statusCode, httpVersion: nil, headerFields: nil)
        var data: Data! = Data()
        
        //1: test url connection
        var error = Errors.handle(nil, response: nil, error: noInternetError)
        XCTAssertEqual(error, Errors.noInternetConnection)
        
        //2: test if the response is 200
        error = Errors.handle(nil, response: urlResponse, error: randomError)
        XCTAssertEqual(error, Errors.invalidStatusCode(statusCode))
        
        //3: test when call fails
        error = Errors.handle(data, response: nil, error: acceptedCodeError)
        XCTAssertEqual(error, Errors.returnedError(acceptedCodeError))

        //4: test when server returns nothing
        error = Errors.handle(data, response: nil, error: nil)
        XCTAssertEqual(error, Errors.dataReturnedNil)

        //5: test when when everything is fine
        data = String("hi").data(using: .utf8)
        error = Errors.handle(data, response: nil, error: nil)
        XCTAssertEqual(error, nil)
    }
}
