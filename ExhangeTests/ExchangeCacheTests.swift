//
//  ExchangeCacheTests.swift
//  ExhangeTests
//
//  Created by Hady Nourallah on 19/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import XCTest
@testable import Currencies

class ExchangeCacheTests: XCTestCase {
    let cache = try? HNCache()
    let key = "AUDEUR"

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        try? self.cache?.delete(key)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testInsertingDataNoAsync() {
        let exchange = ExchangeResponse(name: "AUDEUR", value: 14.0)
        let x = cache?.persist(item: exchange)
        let loaded: ExchangeResponse? = cache?.load(primaryKey: key)
        XCTAssertEqual(loaded, exchange)
        XCTAssertEqual(x?.0?.lastPathComponent, key)
        XCTAssertNil(x?.1)
    }
    
    func testInsertingDataWithAsync() {
        let expectation = XCTestExpectation(description: "Cache success")
        let exchange = ExchangeResponse(name: "AUDEUR", value: 14.0)
        cache?.persist(item: exchange, completion: { (url, error) in
            XCTAssertEqual(url?.lastPathComponent, self.key)
            XCTAssertNil(error)
            
            let loaded: ExchangeResponse? = self.cache?.load(primaryKey: self.key)
            XCTAssertEqual(loaded, exchange)

            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testUpdateDataNoAsync() {
        
        let rate = ExchangeResponse(name: "AUDEUR", value: 14.0)
        let _ = cache?.persist(item: rate)
        var loaded: ExchangeResponse?
        loaded = cache?.load(primaryKey: key)
        XCTAssertEqual(loaded, rate)
        
        let rate1 = ExchangeResponse(name: "AUDEUR", value: 14.5)
        let x1 = cache?.persist(item: rate1)
        loaded = cache?.load(primaryKey: key)
        XCTAssertEqual(loaded, rate1)
        XCTAssertEqual(14.5, rate1.value)
        XCTAssertNil(x1?.1)
    }
    
    func testUpdateWithAsync() {
        let expectation = XCTestExpectation(description: "Cache success")

        let rate = ExchangeResponse(name: "AUDEUR", value: 14.0)
        let rate1 = ExchangeResponse(name: "AUDEUR", value: 14.5)
        cache?.persist(item: rate, completion: nil)
        cache?.persist(item: rate1, completion: { (_, _) in
            let loaded: ExchangeResponse? = self.cache?.load(primaryKey: self.key)
            
            XCTAssertEqual(loaded, rate1)
            XCTAssertEqual(14.5, rate1.value)
            
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testDelete() {
        self.testInsertingDataNoAsync()
        XCTAssertNoThrow(try self.cache?.delete(self.key)) //1: should delete successfully.
        XCTAssertThrowsError((try self.cache?.delete(self.key)))//2: should throw an error no file/directory found.
        let loaded: ExchangeResponse? = self.cache?.load(primaryKey: self.key)
        XCTAssertNil(loaded)
    }
}
