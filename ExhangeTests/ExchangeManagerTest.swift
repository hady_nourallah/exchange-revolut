//
//  ExchangeManagerTest.swift
//  ExhangeTests
//
//  Created by Hady Nourallah on 19/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import XCTest
@testable import Currencies
class ExchangeManagerTest: XCTestCase {
    let pairs: [String] = ["AUDEUR", "EURAUD"]
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        pairs.forEach({try? HNCache().delete($0)})
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDataFetching() {
        let expectation: XCTestExpectation = XCTestExpectation(description: "Fetch data success")
        ExchangeManager().fetchRates(for: pairs) { (success, error) in
            XCTAssertEqual(success, true)
            XCTAssertNil(error)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 1.5) //1.5 because of considering fetching data, map it, then caching it
    }
    
    func testDataLoadingFromCache() {
        self.testDataFetching()
        let responses = ExchangeManager().loadRates(for: pairs)
        XCTAssertEqual(responses.map({$0.name}).sorted(), pairs.sorted())
    }
    
    func testGetDataFlow() {
        //the flow goes like this
        //first load from Cache
        //then fetch, and load from cache
        
        let expectation: XCTestExpectation = XCTestExpectation(description: "Fetch data success")
        var times: Int = 0
        ExchangeManager().getRates(for: pairs) { (rates, error) in
            if times == 0 {
                XCTAssert(rates == []) //first time cache is empty to returns empty
            } else {
                XCTAssertEqual(rates.map({$0.name}).sorted(), self.pairs.sorted())
            }
            times += 1
            if times > 1 {
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 1.5) //1.5 because of considering fetching data, map it, then caching it
    }
    
    func testTimerFlow() {
        let expectation: XCTestExpectation = XCTestExpectation(description: "Fetch data success")
        var retries: Int = 0
        ExchangeManager().getRates(for: self.pairs, every: 1) { (rates, error) in
            if retries == 0 {
                XCTAssertEqual(rates, []) //first time cache is empty hence the empty return
            } else {
                XCTAssertEqual(rates.map({$0.name}).sorted(), self.pairs.sorted())
            }
            retries += 1
            if retries >= 10 {
                expectation.fulfill()
            }
            
        }
        
        wait(for: [expectation], timeout: 10)
    }
    
    func testTimerFlowPerformance() {
        self.measure {
            self.setUp()
            self.testTimerFlow()
        }
    }
    
    func testTimersusbendsAfterMaxTriesIsFulfilled() {
        var retries: Int = 0
        let expectation: XCTestExpectation = XCTestExpectation(description: "Fetch data success")
        let maxRetries: Int = Int.random(in: 5...10)
        ExchangeManager().getRates(for: ["wrong", "pair"], every: 1, retry: 5) { (rates, error) in
            retries += 1
            if retries > maxRetries {
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: Double(maxRetries) * 1.5)
    }
}
