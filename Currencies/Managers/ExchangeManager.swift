//
//  ExchangeManager.swift
//  Currencies
//
//  Created by Hady Nourallah on 16/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import Foundation

class ExchangeManager {
    private let cache: HNCache? = try? HNCache()
    private(set) static var timer: HNTimer?
    let maxTries: Int = 10
    
    //MARK:- Fetch data
    func fetchRates(for pairs: [String], response: @escaping (Bool, Errors?) -> ()) {
        Service().getCurrentExchange(pairs) { (rates, error) in
            if let error = error {
                response(false, error)
            } else {
                rates?.forEach({ _ = self.cache?.persist(item: $0) })
                response(true, nil)
            }
        }
    }
    
    //MARK:- Data consistency
    func getRates(for pairs: [String], response: @escaping ([ExchangeResponse], Errors?) -> ()) {
        response(self.loadRates(for: pairs), nil)
        fetchRates(for: pairs) { (success, Errors) in
            response(self.loadRates(for: pairs), nil)
        }
    }
    
    func getRates(for pairs: [String], every seconds: TimeInterval, retry: Int = 0, response: @escaping ([ExchangeResponse], Errors?) -> ()) {
        response(self.loadRates(for: pairs), nil)

        if ExchangeManager.timer == nil || ExchangeManager.timer?.timeInterval != seconds {
            ExchangeManager.timer?.suspend()
            ExchangeManager.timer = HNTimer(seconds)
        }
        
        if pairs.count == 0 {
            ExchangeManager.timer?.suspend()
            response([], nil) //in case sent 0 pairs, suspend timer and return nothing
            return
        }
        
        if retry >= maxTries {
            ExchangeManager.timer?.suspend()
            return
        }
        
        ExchangeManager.timer?.eventHandler = {
            self.fetchRates(for: pairs) { (success, error) in
                if error == nil {
                    self.getRates(for: pairs, every: seconds, retry: retry + 1, response: response)
                } else {
                    response(self.loadRates(for: pairs), error)
                }
            }
        }

        ExchangeManager.timer?.resume()
    }
    
    //MARK:- load cache
    func loadRates(for pairs: [String]) -> [ExchangeResponse] {
        let x = pairs.compactMap({ (pair) -> ExchangeResponse? in  let e: ExchangeResponse? = self.cache?.load(primaryKey: pair); return e})
        return x
    }
}

