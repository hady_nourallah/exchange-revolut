//
//  ExchangeModel.swift
//  Currencies
//
//  Created by Hady Nourallah on 17/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import Foundation

class ExchangeModel {
    let pair: String
    let from: CurrencyModel
    let to: CurrencyModel
    let value: Double
    
    init?(pair: String, value: Double) {
        self.value = value
        self.pair = pair
        let pairs = Array(pair).split()
        if let from = Settings.getCurrency(by: String(pairs.left)),
            let to = Settings.getCurrency(by: String(pairs.right)) {
            self.from = from
            self.to = to
        } else {
            return nil
        }
    }
    
    convenience init?(_ exchangeRate: ExchangeResponse) {
        self.init(pair: exchangeRate.name, value: exchangeRate.value)
    }
}
