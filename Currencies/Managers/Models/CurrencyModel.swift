//
//  CurrencyModel.swift
//  Currencies
//
//  Created by Hady Nourallah on 17/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import Foundation

struct CurrencyModel: Codable {
    var code: String
    var symbol_native: String
    var name: String
    var code_short: String
}
