//
//  AddExchangeViewController.swift
//  Currencies
//
//  Created by Hady Nourallah on 17/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import UIKit

class AddExchangeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var viewModel: ViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = self.viewModel ?? ViewModel()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        showBackButton()
        self.view.accessibilityIdentifier = "AddExchangeViewController"
    }
    
    func showBackButton() {
        if (self.navigationController?.viewControllers.count) ?? 0 > 1 { //if navigation bar has more than root view controller
            self.navigationItem.hidesBackButton = true
            let newBackButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(self.backAction))
            self.navigationItem.leftBarButtonItem = newBackButton
        }
    }
    
    @objc private func backAction() {
        self.viewModel.dropLastSelectedCode()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func close(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil) //precautions step if the ViewController doesn't have navigation controller
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Settings.currencies.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = Settings.currencies[indexPath.row]
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "AddExchangeTableViewCell", for: indexPath) as! AddExchangeTableViewCell
        cell.setupCell(model, isSelected: self.viewModel.isSelected(model.code))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = Settings.currencies[indexPath.row]

        if self.viewModel.select(model.code) {
            if (self.viewModel.shouldAddPair()) {
                self.viewModel.addPair(self.viewModel.selected)
                self.close(self)
            } else {
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "AddExchangeViewController") as! AddExchangeViewController
                viewController.viewModel = self.viewModel
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
        
    }
    
    class ViewModel {
        var selected: String = ""
        
        func select(_ code: String) -> Bool {
            if isSelected(code) {
                return false
            }
            selected.append(code)
            return true
        }
        
        func dropLastSelectedCode() {
            self.selected = String(self.selected.dropLast(3))
        }
        
        func isSelected(_ code: String) -> Bool {
            return selected.contains(code)
//            return selected == code
        }
        
        func shouldAddPair() -> Bool {
            return selected.isPairFormat
        }
        
        func addPair(_ pair: String) {
            Settings.selectedPairs.append(pair)
        }
    }
}
