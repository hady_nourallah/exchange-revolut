//
//  HomeExchangeTableViewCell.swift
//  Currencies
//
//  Created by Hady Nourallah on 17/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import UIKit

class HomeExchangeTableViewCell: UITableViewCell {
    @IBOutlet weak var fromValueLabel: UILabel!
    @IBOutlet weak var fromdetailsLabel: UILabel!
    @IBOutlet weak var toValueLabel: UILabel!
    @IBOutlet weak var todetailsLabel: UILabel!

    func setupCell(_ model: ExchangeModel) {
        self.fromValueLabel.attributedText = ViewModel().getFromLabelText(model)
        self.toValueLabel.attributedText = ViewModel().getToLabelText(model)
        self.fromdetailsLabel.text = ViewModel().getFromLabelDetails(model)
        self.todetailsLabel.text = ViewModel().getToLabelDetails(model)
        self.accessibilityIdentifier = "HomeTableCell: \(model.pair)"
    }
    
    private class ViewModel {
        
        func getFromLabelText(_ model: ExchangeModel) -> NSAttributedString {
            return getAttributedTextForCurrency(1.00,
                                                append: NSAttributedString(string: model.from.symbol_native, attributes: [NSAttributedString.Key.font : UIFont.avenir(20)]))
        }
        
        func getToLabelText(_ model: ExchangeModel) -> NSAttributedString {
            return getAttributedTextForCurrency(model.value,
                                                append: NSAttributedString(string: model.to.symbol_native, attributes: [NSAttributedString.Key.font : UIFont.avenir(20)]))
        }
        
        func getFromLabelDetails(_ model: ExchangeModel) -> String {
            return "\(model.from.name) · \(model.from.code)"
        }

        func getToLabelDetails(_ model: ExchangeModel) -> String {
            return "\(model.to.name) · \(model.to.code)"
        }

        func getAttributedTextForCurrency(_ value: Double, append attributes: NSAttributedString...) -> NSAttributedString {
            let currency = value.toCurrencyFormat(decimalSeperator: " ")
            if currency.isEmpty == false {
                let attributed: NSMutableAttributedString = NSMutableAttributedString(string: currency, attributes: [NSAttributedString.Key.font : UIFont.avenir(20)])
                attributed.addAttributes([NSAttributedString.Key.font : UIFont.avenir(17)], range: NSRange(location: currency.count - 2, length: 2))
                
                attributes.forEach({ attributed.append($0) })
                return attributed
            }
            
            return NSAttributedString()
        }
    }
}
