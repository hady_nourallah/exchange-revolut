
//
//  AddExchangeTableViewCell.swift
//  Currencies
//
//  Created by Hady Nourallah on 17/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import UIKit
import FlagKit

class AddExchangeTableViewCell: UITableViewCell {
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var selectedLayer: UIVisualEffectView!
    
    func setupCell(_ model: CurrencyModel, isSelected: Bool) {
        self.flagImageView.image = Flag(countryCode: model.code_short)?.image(style: .circle)
        self.currencyLabel.text = model.name
        self.selectedLayer.isHidden = !isSelected
        
        
        self.accessibilityIdentifier                = "ExchangeCell: " + model.name
        self.contentView.accessibilityIdentifier    = "ExchangeContentView: " + model.name
        self.flagImageView.accessibilityIdentifier  = "ExchangeImageView" + model.name
        self.currencyLabel.accessibilityIdentifier  = "ExchangeLabel" + model.name
        self.selectedLayer.accessibilityIdentifier  = "ExchangeSelectedLayer" + model.name
        self.selectedLayer.subviews.forEach({ $0.accessibilityIdentifier = "ExchangeSelectedLayer" + model.name})
    }
}
