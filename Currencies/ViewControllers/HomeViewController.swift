//
//  ViewController.swift
//  Currencies
//
//  Created by Hady Nourallah on 15/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import UIKit

protocol HomeViewModelDelegate: class {
    func dataUpdated()
    func showError(_ errorMsg: String)
}

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, HomeViewModelDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = ViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.viewModel.delegate = self
        self.setAccessibilityIdentifier()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.getData()
    }
    
    func setAccessibilityIdentifier() {
        self.view.accessibilityIdentifier = "HomeViewController"
//        self.tableView.accessibilityIdentifier = "HomeTableView"
    }
    
    func dataUpdated() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func showError(_ errorMsg: String) {
        DispatchQueue.main.async {
            Toast.show(errorMsg, view: self.view)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.cells.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellType = self.viewModel.cells[indexPath.row]
        switch cellType {
        case .empty:
            return tableView.frame.height
        case .header, .cell(_):
            return 70
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType = self.viewModel.cells[indexPath.row]
        switch cellType {
        case .empty:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "HomeExchangeEmptyTableViewCell", for: indexPath)
            cell.accessibilityIdentifier = "HomeTableCellEmpty"
            return cell
            
        case .header:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "HomeExchangeHeaderTableViewCell", for: indexPath)
            cell.accessibilityIdentifier = "HomeTableCellHeader"
            return cell

        case .cell(let model):
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "HomeExchangeTableViewCell", for: indexPath) as! HomeExchangeTableViewCell
            cell.setupCell(model)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellType = self.viewModel.cells[indexPath.row]
        switch cellType {
        case .empty, .header:
            if let addPairsFlow = self.storyboard?.instantiateViewController(withIdentifier: "AddExchangeFlowNavigationController") {
                self.present(addPairsFlow, animated: true, completion: nil)
            }
        case .cell(let m):
            let alertController = UIAlertController(title: "Are you sure you want to delete \(m.pair.uppercased())?", message: nil, preferredStyle: .actionSheet)
            alertController.addAction(UIAlertAction(title: "Delete!", style: .default, handler: { (_) in
                self.viewModel.deletePair(m)
            }))
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.show(alertController, sender: nil)
        }
    }
    
    
    
    class ViewModel {
        var cells: [CellType] = [.empty]
        let updateInterval: TimeInterval = 1
        weak var delegate: HomeViewModelDelegate?
        
        func setupCells(_ exchangeModels: [ExchangeModel]) {
            self.cells = []
            
            if exchangeModels.count == 0 {
                cells.append(.empty)
            } else {
                cells.append(.header)
                for m in exchangeModels {
                    cells.append(.cell(m))
                }
            }
            
            self.delegate?.dataUpdated()
        }
        
        func getData() {
            let pairs = Settings.selectedPairs
            ExchangeManager().getRates(for: pairs, every: updateInterval) { (exchanges, error) in
                if let error = error {
                    self.delegate?.showError(error.message)
                } else {
                    let rates = exchanges.compactMap({ExchangeModel($0)})
                    self.setupCells(rates)
                }
            }
        }
        
        func deletePair(_ model: ExchangeModel) {
            Settings.selectedPairs = Settings.selectedPairs.filter({$0 != model.pair})
            getData()
            self.delegate?.dataUpdated()
        }
        
        func deletePair(_ pair: String) {
            Settings.selectedPairs = Settings.selectedPairs.filter({$0 != pair})
            getData()
            self.delegate?.dataUpdated()
        }
        
        enum CellType: Equatable {
            static func == (lhs: HomeViewController.ViewModel.CellType, rhs: HomeViewController.ViewModel.CellType) -> Bool {
                return lhs.raw == rhs.raw
            }
            
            var raw: String {
                get {
                    switch self {
                    case .header:
                        return "header"
                    case .empty:
                        return "empty"
                    case .cell(let m):
                        return "cell \(m.pair)"
                    }
                }
            }
            case header
            case empty
            case cell(_ model: ExchangeModel)
        }
    }
}
