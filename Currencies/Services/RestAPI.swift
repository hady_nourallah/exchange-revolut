//
//  RestAPI.swift
//  Currencies
//
//  Created by Hady Nourallah on 16/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import Foundation

enum RestAPI {
    case getExchange(_ currencies: [String])
}

extension RestAPI {
    var url: URL? {
        get {
            return getURL()
        }
    }
    
    private func getURL() -> URL? {
        switch self {
        case .getExchange(let pairs):
            guard var urlComponents = URLComponents(string: Keys.Service.BASE_URL) else {
                return nil
            }
            
            let pairs = pairs.map({URLQueryItem(name: "pairs", value: $0)})
            urlComponents.queryItems = pairs
            return urlComponents.url
        }
    }
}

extension RestAPI {
    
    var request: URLRequest? {
        get {
            return createRequest()
        }
    }
    
    private func createRequest() -> URLRequest? {
        guard let url = self.url else {
            return nil
        }
        
        switch self {
        case .getExchange(_):
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            return request
        }
    }
}
