//
//  Service.swift
//  Currencies
//
//  Created by Hady Nourallah on 16/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import Foundation

class Service {
    func getCurrentExchange(_ pairs: [String], _ callbackResponse : @escaping ([ExchangeResponse]?, Errors?) -> ()) {
        guard let request = RestAPI.getExchange(pairs).request else {
            callbackResponse(nil, Errors.badURLFormat)
            return
        }
        URLSession(configuration: .ephemeral) //no caching
            .dataTask(with: request) { (data, response, error) in
                let error = Errors.handle(data, response: response, error: error)
                guard let data = data else {
                    callbackResponse(nil, error)
                    return
                }
                
                let decoder = JSONDecoder()
                decoder.userInfo = [.pairs: pairs.joined(separator: ",")]
                let rates = try? decoder.decode(Exchanges.self, from: data).rates
                callbackResponse(rates, error)
            }
            .resume()
    }
}
