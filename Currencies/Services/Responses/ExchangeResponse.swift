//
//  Exchange.swift
//  Currencies
//
//  Created by Hady Nourallah on 16/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import Foundation

struct ExchangeResponse: Codable, HNCachable, Equatable {
    var primaryKey: String {
        return name
    }
    
    let name: String
    let value: Double
}

struct Exchanges: Decodable {
    var rates: [ExchangeResponse] = []
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CurrencyCodingKeys.self)
        let currentPairs: String = decoder.userInfo[.pairs] as? String ?? ""
        let pairs = currentPairs.split(separator: ",")
        for pair in pairs {
            let value = try container.decode(Double.self, forKey: CurrencyCodingKeys.create(key: String(pair)))
            self.rates.append(ExchangeResponse(name: String(pair), value: value))
        }
    }
    
}

//Create generic currency Coding keys
struct CurrencyCodingKeys: CodingKey, ExpressibleByStringLiteral {
    // MARK: CodingKey
    var stringValue: String
    var intValue: Int?
    
    init?(stringValue: String) { self.stringValue = stringValue }
    init?(intValue: Int) { return nil }
    
    // MARK: ExpressibleByStringLiteral
    typealias StringLiteralType = String
    init(stringLiteral: StringLiteralType) { self.stringValue = stringLiteral }
    
    static func create(key: String) -> CurrencyCodingKeys {
        return CurrencyCodingKeys(stringLiteral: key)
    }
}

extension CodingUserInfoKey {
    static let pairs = CodingUserInfoKey(rawValue: "currentCurrencies")!
}

