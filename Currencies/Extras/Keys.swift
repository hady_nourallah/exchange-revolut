//
//  Keys.swift
//  Currencies
//
//  Created by Hady Nourallah on 16/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import Foundation
import UIKit

struct Keys {
    struct Service {
        static let BASE_URL: String = "https://europe-west1-revolut-230009.cloudfunctions.net/revolut-ios"
    }
    
    static let SELECTED_PAIRS: String = "SELECTED_PAIRS_KEY"
}

extension UIFont {
    static func avenir(_ size: CGFloat) -> UIFont { return UIFont(name: "Avenir-Heavy", size: size)! }
}
