//
//  Numbers+Extensions.swift
//  Currencies
//
//  Created by Hady Nourallah on 17/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import Foundation

extension Double {
    func toCurrencyFormat(decimalSeperator: String = "") -> String {
        let nf = NumberFormatter()
        nf.numberStyle = .currency
        nf.currencySymbol = ""
        nf.currencyDecimalSeparator = decimalSeperator
        
        return nf.string(from: NSNumber(value: self)) ?? ""
    }
}
