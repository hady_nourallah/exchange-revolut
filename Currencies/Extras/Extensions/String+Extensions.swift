//
//  String+Extensions.swift
//  Currencies
//
//  Created by Hady Nourallah on 17/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import Foundation

extension String {
    
    var isPairFormat: Bool {
        get {
            return self.count == 6 //simple validation for pair 
        }
    }
}
