//
//  HNCacher.swift
//  Currencies
//
//  Created by Hady Nourallah on 16/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import Foundation

final class HNCache {
    let destination: URL
    private let queue = OperationQueue()
    
    init(_ cacheLoc: String? = nil ) throws {
        let documentFolder = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        self.destination = URL(fileURLWithPath: documentFolder).appendingPathComponent(cacheLoc ?? "HNCacherDefault", isDirectory: true)
        try FileManager.default.createDirectory(at: self.destination, withIntermediateDirectories: true, attributes: nil)
    }
    
    //using callback
    func persist(item: HNCachable, completion: ((_ url: URL?, _ error: Error?) -> Void)?) {
        var url: URL?
        var error: Error?
        // Create an operation to process the request.
        let operation = BlockOperation {
            do {
                url = try self.persist(data: item.transform(), for: item.primaryKey )
            } catch let persistError {
                error = persistError
            }
        }
        // Set the operation's completion block to call the request's completion handler.
        operation.completionBlock = {
            completion?(url, error)
        }
        // Add the operation to the queue to start the work.
        queue.addOperation(operation)
    }
    
    func persist(item: HNCachable) -> (URL?, Error?) {
        var url: URL?
        var error: Error?

        do {
            url = try self.persist(data: item.transform(), for: item.primaryKey )
        } catch let persistError {
            error = persistError
        }
        
        return (url, error)
    }
    
    private func persist(data: Data, for primaryKey: String) throws -> URL {
        do {
            let d = self.destination.appendingPathComponent(primaryKey, isDirectory: false)
            try? delete(primaryKey) //it will fail in case file doesn't case
            try data.write(to: d, options: [.atomic])
            return d
        } catch let error {
            throw error
        }
    }

    
    func load<T: HNCachable & Codable>(primaryKey: String) -> T? {
        guard
            let data = try? Data(contentsOf: destination.appendingPathComponent(primaryKey, isDirectory: false)),
            let decoded = try? JSONDecoder().decode(T.self, from: data)
            else { return nil }
        return decoded
    }

    func delete(_ primaryKey: String) throws {
        let d = destination.appendingPathComponent(primaryKey, isDirectory: false)
        try FileManager.default.removeItem(at: d)
    }
}

protocol HNCachable {
    var primaryKey: String { get }
    func transform() -> Data
}

extension HNCachable where Self: Codable {
    func transform() -> Data {
        do {
            let encoded = try JSONEncoder().encode(self)
            return encoded
        } catch let error {
            fatalError("Unable to encode object: \(error)")
        }
    }
}
