//
//  Errors.swift
//  Currencies
//
//  Created by Hady Nourallah on 16/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import Foundation

public enum Errors: Error {
    case noInternetConnection
    case dataReturnedNil
    case returnedError(Error)
    case invalidStatusCode(Int)
    case badURLFormat
    case customError(String)
}

extension Errors {
    static func handle(_ data: Data?, response: URLResponse?, error: Error?) -> Errors? {
        if error?._domain == NSURLErrorDomain && error?._code == NSURLErrorNotConnectedToInternet {
            return .noInternetConnection
        } else if let response = response as? HTTPURLResponse, response.statusCode != 200 {
            return .invalidStatusCode(response.statusCode)
        } else if let error = error {
            return .returnedError(error)
        } else if data == nil || data?.count == 0 {
            return .dataReturnedNil
        }
        
        return nil
    }
}

extension Errors {
    public var message: String {
        return getMessage()
    }
    
    private func getMessage() -> String {
        switch self {
        case .customError(let errorMessage):
            return errorMessage
        case .dataReturnedNil:
            return "Data seems to be empty!"
        case .invalidStatusCode(let code):
            return "Difficulties communicating with the Server, code: \(code)"
        case .noInternetConnection:
            return "Please connect to the internet"
        case .badURLFormat:
            return "URL Format unrecognized"
        case .returnedError(let e):
            return "Random Error Appeared!!\n description = \(e.localizedDescription)"
        }
    }
}

extension Errors: Equatable {
    public static func == (lhs: Errors, rhs: Errors) -> Bool {
        return lhs.message == rhs.message
    }
    
    
}
