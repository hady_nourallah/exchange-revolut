//
//  Settings.swift
//  Currencies
//
//  Created by Hady Nourallah on 16/07/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import Foundation

class Settings {
    static private var _currencies: [CurrencyModel] = []
    static var currencies: [CurrencyModel] {
        get {
            if (_currencies.isEmpty == true) {
                //load to _currencies
                let loaded: [CurrencyModel]? = BaseUtils.decodeJSONFile("currencies", [CurrencyModel].self)
                _currencies = loaded ?? []
                
            }
            
            return _currencies
        }
    }
    
    static func getCurrency(by code: String) -> CurrencyModel? {
        return currencies.filter({$0.code == code}).first
    }
    
    static var selectedPairs: [String] {
        get {
            return UserDefaults.standard.stringArray(forKey: Keys.SELECTED_PAIRS)?.sorted() ?? []
        }
        
        set {
            //used Set to keep the pairs unique, but it loses the order of pairs, 'cause of hashing
            let set: Set<String> = Set<String>(newValue)
            let v: [String] = Array(set)
            UserDefaults.standard.set(v, forKey: Keys.SELECTED_PAIRS)
            _ = UserDefaults.standard.synchronize()
        }
    }
}

