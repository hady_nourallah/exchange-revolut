//
//  Toast.swift
//  BitcoinNow
//
//  Created by Hady Nourallah on 30/04/2019.
//  Copyright © 2019 Hady. All rights reserved.
//

import UIKit

class Toast: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak fileprivate var messageLabel: UILabel!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("Toast", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    static func show(_ message: String, view: UIView) {
        let v = Toast(frame: CGRect(x: 8, y: view.frame.height - 100, width: view.frame.width - 20, height: 60))
        v.messageLabel.text = message
        v.alpha = 0
        view.addSubview(v)
        UIView.animate(withDuration: 0.5) {
            v.alpha = 1
        }
        
        Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { (_) in
            hide(view)
        }
    }
    
    static func hide(_ view: UIView) {
        view.subviews.filter({ $0 is Toast }).forEach { (v) in
            hideView(v)
        }
    }
    
    static func hide(_ toast: Toast) {
        hideView(toast)
    }
    
    private static func hideView(_ v: UIView) {
        UIView.animate(withDuration: 0.5, animations: {
            v.alpha = 0
        }) { (_) in
            v.removeFromSuperview()
        }
    }
}

